function execute(word1, word2) {
  if (typeof word1 !== 'string') throw new Error('First parameter should be a string');
  if (typeof word2 !== 'string') throw new Error('Second parameter should be a string');
  if (word1.length === 0) throw new Error('First parameter required');
  if (word2.length === 0) throw new Error('Second parameter required');

  console.time('execution time');
  console.log('input:', word1);

  let map = [];

  for (let i = 0, letter1; letter1 = word1[i]; i++) {
    for (let j = 0, letter2; letter2 = word2[j]; j++) {
      if (letter1 === letter2) {
        map[j] = { letter: letter1, j };
      }
    }
  }

  let tmpWord = [];

  for (let i = 0, obj; obj = map[i]; i++) {
    tmpWord[obj.j] = obj.letter;
  }

  for (let i = 0; i < tmpWord.length; i++) {
    if (tmpWord[i] === undefined) {
      tmpWord[i] = word2[i];
    }
  }

  let resultWord = tmpWord.join('');

  if (resultWord === word2) {
    console.log('output:', resultWord);
    console.timeEnd('execution time');
    return resultWord;
  } else {
    for (let i = 0, letter1; letter1 = word2[i]; i++) {
      if (i > tmpWord.length - 1) {
        tmpWord.push(letter1);
      }
    }

    resultWord = tmpWord.join('');

    if (resultWord === word2) {
      console.log('output:', resultWord);
      console.timeEnd('execution time');
      return resultWord;
    } else {
      throw new Error(`Cant't convert word1 to word2`);
    }
  }
}

execute('sushi', 'ihsus');